package com.vv.parkinglot.repository;

import com.vv.parkinglot.model.ParkingLot;
import com.vv.parkinglot.model.ParkingSize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingLotRepository extends JpaRepository<ParkingLot,Long> {
    ParkingLot findFirstBySizeAndVehicleIsNull(ParkingSize size);
    int countByVehicleIsNotNull();
}
