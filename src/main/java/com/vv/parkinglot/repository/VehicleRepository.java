package com.vv.parkinglot.repository;

import com.vv.parkinglot.model.History;
import com.vv.parkinglot.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Vehicle findByLicensePlate(String licensePlate);
//    List<Vehicle> findVehicle(String licensePlate);
//    public List<Vehicle> search(String licensePlate);
}
