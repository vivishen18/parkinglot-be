package com.vv.parkinglot.repository;

import com.vv.parkinglot.model.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History, Long> {
    List<History> findByVehicleLicensePlateOrderByTimestamp(String licensePlate);
}