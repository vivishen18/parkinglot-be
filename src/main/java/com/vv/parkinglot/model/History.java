package com.vv.parkinglot.model;
import javax.persistence.*;
import java.time.LocalDateTime;
@Entity
@Table(name= "history")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;

    private LocalDateTime timestamp;

    private String event;


    public History(Long id, Vehicle vehicle, LocalDateTime timestamp, String event) {
        this.id = id;
        this.vehicle = vehicle;
        this.timestamp = timestamp;
        this.event = event;
    }

    public History() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;

    }
}
