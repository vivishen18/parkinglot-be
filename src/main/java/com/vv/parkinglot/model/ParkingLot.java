package com.vv.parkinglot.model;

import javax.persistence.*;
@Entity
@Table(name="parking_lot")
public class ParkingLot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private ParkingSize size;
    @OneToOne(mappedBy = "parking")
    private Vehicle vehicle;

    public ParkingLot(Long id, ParkingSize size, Vehicle vehicle) {
        this.id = id;
        this.size = size;
        this.vehicle = vehicle;
    }

    public ParkingLot() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ParkingSize getSize() {
        return size;
    }

    public void setSize(ParkingSize size) {
        this.size = size;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }


}
