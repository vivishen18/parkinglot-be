package com.vv.parkinglot.service;

import com.vv.parkinglot.model.*;
import com.vv.parkinglot.repository.HistoryRepository;
import com.vv.parkinglot.repository.ParkingLotRepository;
import com.vv.parkinglot.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ParkingService {

    private final int TOTAL_PARKING_SPACES = 20;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private HistoryRepository historyRepository;

    public ParkingLot parkVehicle(Vehicle vehicle) {
        ParkingLot parking = null;
        if (vehicle.getType() == VehicleType.BIKE) {
            parking = parkingLotRepository.findFirstBySizeAndVehicleIsNull(ParkingSize.S);

            if (parking == null) {
                parking = parkingLotRepository.findFirstBySizeAndVehicleIsNull(ParkingSize.M);

            }
        }else if (vehicle.getType() == VehicleType.CAR) {
                parking = parkingLotRepository.findFirstBySizeAndVehicleIsNull(ParkingSize.M);
            }

        if (parking != null) {
            vehicle.setParking(parking);
            vehicleRepository.save(vehicle);
            History history = new History();
            history.setVehicle(vehicle);
            history.setTimestamp(LocalDateTime.now());
            history.setEvent("PARKED");
            historyRepository.save(history);
            return parking;
        }
        return null;
    }

//        ParkingLot parking = parkingLotRepository.findFirstBySizeAndVehicleIsNull(ParkingSize.M);
//        if (parking != null) {
//            vehicle.setParking(parking);
//            vehicleRepository.save(vehicle);
//            return parking;
//        }


    public void unparkVehicle(String licensePlate) {
        Vehicle vehicle = vehicleRepository.findByLicensePlate(licensePlate);
        if (vehicle != null) {
            vehicle.setParking(null);
            vehicleRepository.save(vehicle);
            History history = new History();
            history.setVehicle(vehicle);
            history.setTimestamp(LocalDateTime.now());
            history.setEvent("UNPARKED");
            historyRepository.save(history);
        }
    }
    public List<History> getHistory() {
        return historyRepository.findAll();
    }

    public List<Vehicle> getVehicleHistory() {
        return vehicleRepository.findAll();
    }

    public int getAvailableParkingSpaces() {
        return TOTAL_PARKING_SPACES - parkingLotRepository.countByVehicleIsNotNull();
    }
    public List<History> findByVehicleLicensePlateOrderByTimestamp(){
        return historyRepository.findAll();

    }
//     public List<Vehicle> findVehicle(){
//        return vehicleRepository.findAll();
//     }

}

