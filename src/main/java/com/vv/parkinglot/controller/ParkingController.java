package com.vv.parkinglot.controller;

import com.vv.parkinglot.model.History;
import com.vv.parkinglot.model.ParkingLot;
import com.vv.parkinglot.model.Vehicle;
import com.vv.parkinglot.repository.VehicleRepository;
import com.vv.parkinglot.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parking")
@CrossOrigin
public class ParkingController {

    @Autowired
    private ParkingService parkingService;
    @Autowired
    VehicleRepository vehicleRepository;

    @PostMapping("/park")
    public ResponseEntity<ParkingLot> parkVehicle(@RequestBody Vehicle vehicle) {
        ParkingLot parking = parkingService.parkVehicle(vehicle);
        if (parking != null) {
            return ResponseEntity.ok(parking);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/unpark")
    public ResponseEntity<Void> unparkVehicle(@RequestParam String licensePlate) {
        parkingService.unparkVehicle(licensePlate);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/history")
    public ResponseEntity<List<History>> getVehicleHistory() {
        List<History> vehicleHistory = parkingService.findByVehicleLicensePlateOrderByTimestamp();
        return ResponseEntity.ok(vehicleHistory);
    }
//    @GetMapping("/vehicle")
//    public ResponseEntity<List<Vehicle>> findVehicleAll() {
//        List<Vehicle> vehicle = parkingService.findVehicle();
//        return ResponseEntity.ok(vehicle);
//    }

    @GetMapping("/available")
    public ResponseEntity<Integer> getAvailableParkingSpaces() {
        int availableParkingSpaces = parkingService.getAvailableParkingSpaces();
        return ResponseEntity.ok(availableParkingSpaces);
    }

    @GetMapping("/find-all")
    public Iterable<Vehicle> findAll() {
        return vehicleRepository.findAll();
    }


//    @GetMapping("/available-spaces")
//    public ResponseEntity<String> getAvailableSpaces{
//        List<ParkingLot> parkingLots = parkingService.getAvailableParkingSpaces();
//        int
//    }

}

